import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { ScheduleGamePage } from '../schedule-game/schedule-game';
import { ChatPage } from '../chat/chat';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';



@Component({
  selector: 'page-play-game',
  templateUrl: 'play-game.html',
})
export class PlayGamePage {

  email: string;
  items;
  new_items;

  constructor(public db: AngularFireDatabase,private fire:AngularFireAuth,public navCtrl: NavController, public navParams: NavParams) {

    this.db.list('/user_profile').valueChanges().subscribe((datas) => {
      this.items = datas
      this.new_items = JSON.stringify(this.items)
      console.log("datas", datas)
      console.log("Current User is", this.fire.auth.currentUser.email);
   

    }, (err) => {
      console.log("probleme; ", err)
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlayGamePage');
  }



  schedule(id,username){
    console.log('ID in schedule IS', id);
     this.navCtrl.push(ScheduleGamePage,{value: id,player_username: username})
  }

  chat(id,username){
    console.log('ID IN CHAT PLAY GAME IS', id);
    this.navCtrl.push(ChatPage, {value: id,player_username: username})
  }

}
