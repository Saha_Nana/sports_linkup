import { Component } from '@angular/core';
import { NavController,AlertController,LoadingController } from 'ionic-angular';
import { PlayGamePage } from '../play-game/play-game';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  email: string;
  items;
  new_items;

  constructor(public loadingCtrl: LoadingController,public alertCtrl: AlertController,public db: AngularFireDatabase,private fire:AngularFireAuth,public navCtrl: NavController) {

    this.db.list('/user_profile').valueChanges().subscribe((datas) => {
      this.items = datas
      this.new_items = JSON.stringify(this.items)
      console.log("datas", datas)
      console.log("Current User is", this.fire.auth.currentUser.email);
    

    }, (err) => {
      console.log("probleme; ", err)
    });
    
  }

  alert(message: string){
    this.alertCtrl.create({
        title: "Info!",
        subTitle: message,
        buttons: ["OK"]
    }).present();
  }




  play(){
    let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });
    loader.present();

this.navCtrl.push(PlayGamePage)
loader.dismiss();

  }

  error(){
    this.alert("Feature not developed yet");

  }

}
