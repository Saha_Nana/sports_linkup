import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { SportSettingsPage } from '../sport-settings/sport-settings';



@Component({
  selector: 'page-pick-sports',
  templateUrl: 'pick-sports.html',
})
export class PickSportsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PickSportsPage');
  }


  sport(){

    this.navCtrl.push(SportSettingsPage)
  }

}
