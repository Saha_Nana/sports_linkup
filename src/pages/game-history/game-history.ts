import { Component } from '@angular/core';
import { NavController, NavParams,ViewController,ModalController } from 'ionic-angular';
import { Renderer } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';


@Component({
  selector: 'page-game-history',
  templateUrl: 'game-history.html',
})
export class GameHistoryPage {
  from_play_game: any;
  player_username;
  current_user_email;

  items;
  new_items;
  check;
  username;
  opponent_name;
  user_scores;
  opponent_scores;
  date;
  location;
  check2;

  constructor(public db: AngularFireDatabase,private fire:AngularFireAuth,public navCtrl: NavController, public navParams: NavParams, public renderer: Renderer, public viewCtrl: ViewController) {

      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);

      this.current_user_email = this.fire.auth.currentUser.email
    this.from_play_game = this.navParams.get("value")
    this.player_username = this.navParams.get("player_username")
    this.location = this.navParams.get("location")
    this.date = this.navParams.get("date")

    console.log("OPPONENT ID",this.from_play_game )
    console.log("player_username ID",this.player_username )

    this.check = this.db.list('user_profile', ref => ref.orderByChild('email').equalTo(this.current_user_email));
   
    this.check.valueChanges().subscribe(
  
      (datas) => { 
       this.items = datas
       this.username = datas[0].username
       console.log("User Profile datas USername", datas[0].username) }
    );

    this.check2 = this.db.list('scores', ref => ref.orderByChild('email').equalTo(this.current_user_email));
   
     this.check2.valueChanges().subscribe(
   
       (datas) => { 
        this.items = datas
        this.user_scores = datas[0].user_scores
        this.opponent_scores = datas[0].opponent_scores
        console.log("User Profile datas USername", datas[0].user_scores) }
     );

      

  }

  

}
