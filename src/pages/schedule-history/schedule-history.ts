import { Component } from '@angular/core';
import { NavController, NavParams,ViewController,ModalController } from 'ionic-angular';
import { GameHistoryPage } from '../game-history/game-history';
import { PutScoresPage } from '../put-scores/put-scores';
// import { Renderer } from '@angular/core';

// import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';




@Component({
  selector: 'page-schedule-history',
  templateUrl: 'schedule-history.html',
})
export class ScheduleHistoryPage {
  // menu_items: Observable<any[]>;
  items;
  new_items;
  current_user_email;
  check;
  username;
  opponent_name;
  
  constructor( public db: AngularFireDatabase,private fire:AngularFireAuth,public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    //  db.list('/user_profile/').valueChanges();
    this.current_user_email = this.fire.auth.currentUser.email

    this.check = this.db.list('game_scheduled', ref => ref.orderByChild('email').equalTo(this.current_user_email));
   
     this.check.valueChanges().subscribe(
   
       (datas) => { 
        this.items = datas
        this.opponent_name = datas[0].opponent_name
        console.log("Scheduled games datas", datas) }
     );

     this.check = this.db.list('user_profile', ref => ref.orderByChild('email').equalTo(this.current_user_email));
   
     this.check.valueChanges().subscribe(
   
       (datas) => { 
        this.items = datas
        this.username = datas[0].username
        console.log("User Profile datas USername", datas[0].username) }
     );

  //  this.db.list('/user_profile').valueChanges().subscribe((datas) => { 
  //    this.items = datas
  //    this.new_items = JSON.stringify(this.items)
  //     console.log("datas", datas)
  //     console.log("items IOD", this.items[0].id)
  //     console.log("items string", JSON.stringify(this.items))
  //   },(err)=>{
  //      console.log("probleme; ", err)
  //   });
 
  }

 


  view(name, opponent_id,location,date){
  //  this.navCtrl.push(GameHistoryPage,{value: opponent_id,player_username: name,location:location,date: date})
   let profileModal1 = this.modalCtrl.create(GameHistoryPage,{value: opponent_id,player_username: name,location:location,date: date})
   profileModal1.present();
  }

  record(name, opponent_id,location,date){
    // this.navCtrl.push(GameHistoryPage)
    console.log('ID IN HISTORY PLAY GAME IS', opponent_id);
    console.log('Name IN HISTORY PLAY GAME IS', name);

    let profileModal = this.modalCtrl.create(PutScoresPage,{value: opponent_id,player_username: name,location:location,date: date});
    profileModal.present();


  }

}
