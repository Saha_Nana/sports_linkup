import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { ScheduleHistoryPage } from '../schedule-history/schedule-history';
import { PlayGamePage } from '../play-game/play-game';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  // tab2Root = ScheduleHistoryPage;
  tab3Root = ScheduleHistoryPage;
  tab4Root = PlayGamePage;

  constructor() {

  }
}
