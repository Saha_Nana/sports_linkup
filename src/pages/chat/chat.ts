import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import 'rxjs/Rx';



// @IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  message;
  mess: object[] = [];
  items;
  new_items;
  username;
  profile;
  from_play_game: any;
  current_user_email;


  surveys2 : Observable<any>; 

  check;
  player_username;


  constructor(public db: AngularFireDatabase,private fire:AngularFireAuth,public navCtrl: NavController, public navParams: NavParams) {
   this.current_user_email = this.fire.auth.currentUser.email
    this.from_play_game = this.navParams.get("value")
    this.player_username = this.navParams.get("player_username")
    console.log("ID IN CHAT IS ", this.from_play_game) 
    
    // this.db.list('chats/').valueChanges().subscribe((datas) => { 
    //   this.items = datas
    //   console.log("DATA; ", this.items) 
     
    //  },(err)=>{
    //     console.log("probleme; ", err)
    //  });

    this.check = this.db.list('user_profile', ref => ref.orderByChild('email').equalTo(this.current_user_email));
   
     this.check.valueChanges().subscribe(
   
       (datas) => { 
        this.items = datas
        this.username = datas[0].username
        console.log("datas", datas[0].username) }
     );

     

     this.check = this.db.list('chats', ref => ref.orderByChild('recipient_id').equalTo(this.from_play_game));
   
     this.check.valueChanges().subscribe(
   
       (datas) => { 
        this.items = datas
        console.log("datas", datas) }
     );

    //  this.db.list('/user_profile').valueChanges().subscribe((datas) => {
    //   this.profile = datas
    

    // }, (err) => {
    //   console.log("probleme; ", err)
    // });

    
    
  }


  sendMessage(){
    
    const user_data = {
      username: this.username,
      message: this.message,
      email: this.current_user_email,
      recipient_id: this.from_play_game
   };
    
   this.db.list('chats').push(user_data).then( () =>{
    console.log(this.message);

    });

    this.message = " "
    

  }


 

}
