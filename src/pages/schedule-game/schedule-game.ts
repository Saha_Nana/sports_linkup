import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ScheduleHistoryPage } from '../schedule-history/schedule-history';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';


@Component({
  selector: 'page-schedule-game',
  templateUrl: 'schedule-game.html',
})
export class ScheduleGamePage {
  location;
  date;
  time;
  hideMe = false;
  from_play_game: any;
  player_username;
  current_user_email;

  constructor(public alertCtrl: AlertController, public loadingCtrl: LoadingController, public db: AngularFireDatabase, private fire: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {

    this.current_user_email = this.fire.auth.currentUser.email
    this.from_play_game = this.navParams.get("value")
    this.player_username = this.navParams.get("player_username")

    console.log("OPPONENT ID", this.from_play_game)
    console.log("player_username ID", this.player_username)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScheduleGamePage');
    console.log("INhere")
  }

  hide() {
    this.hideMe = true;
    console.log("INhere")
  }

  alert(message: string) {
    this.alertCtrl.create({
      title: "Info!",
      subTitle: message,
      buttons: ["OK"]
    }).present();
  }

  save() {
    let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });
    loader.present();

    const user_data = {
      location: this.location,
      date: this.date + " " + this.time,
      email: this.current_user_email,
      opponent_id: this.from_play_game,
      opponent_name: this.player_username
    };

    this.db.list('game_scheduled').push(user_data).then(() => {
      this.alert("Game Scheduled Successfully!");
      this.navCtrl.push(ScheduleHistoryPage)
      loader.dismiss();

    });

  }


}
