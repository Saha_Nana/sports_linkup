import { Component,ViewChild } from '@angular/core';
import {  NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
// import { PickSportsPage } from '../pick-sports/pick-sports';
// import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
// import * as firebase from 'firebase';




@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  opt: string = "signin";
  @ViewChild("username") user;
  @ViewChild("password") password;
  @ViewChild("fullname") fullname;
  @ViewChild("mobile_number") mobile;
  @ViewChild("email") email;
  user_id: any;
  

  // public userProfileRef:firebase.database.Reference;

  constructor(public loadingCtrl: LoadingController,public db: AngularFireDatabase,public alertCtrl: AlertController,private fire:AngularFireAuth, public navCtrl: NavController, public navParams: NavParams) {
    // this.userProfileRef = firebase.database().ref('/userProfile');
  }

  alert(message: string){
    this.alertCtrl.create({
        title: "Info!",
        subTitle: message,
        buttons: ["OK"]
    }).present();
  }


  login(){
    let loader = this.loadingCtrl.create({
      content: "logging in ..."

    });

    loader.present();
    this.fire.auth.signInWithEmailAndPassword(this.email.value, this.password.value)
    .then(data=>{
      console.log("Current User Email is", this.fire.auth.currentUser.email);
        console.log("got data", data);
        loader.dismiss();
        this.alert("Login Successful");
        this.navCtrl.push(TabsPage)
    })
    .catch(error=>{
      console.log("gotan error", error);
      loader.dismiss();
      this.alert(error.message);
    });

    console.log("Would register with", this.email.value, this.password.value);
   

  }



  
  signup(){
    let loader = this.loadingCtrl.create({
      content: "Signing up ..."

    });
    loader.present();

    this.fire.auth.createUserAndRetrieveDataWithEmailAndPassword(this.email.value, this.password.value)
     .then(data=>{
      console.log("Current User is", this.fire.auth.currentUser);
        console.log("User ID", data.user.uid);
        this.alert("Login Successful");
        const user_data = {
           username: this.user.value,
           email: this.email.value,
           fullname: this.fullname.value,
          //  mobile: this.mobile.value,
          id: data.user.uid
        };
     let dbb =  this.db.list('user_profile').push(user_data);
    console.log("gg",dbb);
    // console.log("gg",this.mobile.value);
    loader.dismiss();
    this.navCtrl.push(LoginPage)
    
  })
    .catch(error=>{
      console.log("gotan error", error);
      loader.dismiss();
      this.alert(error.message);
    });

  //   this.fire.auth.createUserAndRetrieveDataWithEmailAndPassword(this.user.value, this.password.value)
  //   .then( newUser => {
  //     this.userProfileRef.child(newUser.user.uid).set({
  //       email: this.user
       
  //     });
  //   })
  //   .catch(error=>{
  //     console.log("gotan error", error);
  //     this.alert(error.message);
  //   });

  //   console.log("Would register with", this.user.value, this.password.value);
  //   // this.navCtrl.push(TabsPage) 

   }

}
