import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { Renderer } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';


@Component({
  selector: 'page-put-scores',
  templateUrl: 'put-scores.html',
})
export class PutScoresPage {
  from_play_game: any;
  player_username;
  current_user_email;

  items;
  new_items;
  check;
  check2;
  username;
  opponent_name;
  user_scores;
  opponent_scores;
  date;
  location;

  constructor(public alertCtrl: AlertController, public loadingCtrl: LoadingController, public db: AngularFireDatabase, private fire: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams, public renderer: Renderer, public viewCtrl: ViewController) {

    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);

    this.current_user_email = this.fire.auth.currentUser.email
    this.from_play_game = this.navParams.get("value")
    this.player_username = this.navParams.get("player_username")
    this.location = this.navParams.get("location")
    this.date = this.navParams.get("date")

    console.log("OPPONENT ID", this.from_play_game)
    console.log("player_username ID", this.player_username)


    this.check = this.db.list('user_profile', ref => ref.orderByChild('email').equalTo(this.current_user_email));

    this.check.valueChanges().subscribe(

      (datas) => {
        this.items = datas
        this.username = datas[0].username
        console.log("User Profile datas USername", datas[0].username)
      }
    );




  }

  alert(message: string) {
    this.alertCtrl.create({
      title: "Info!",
      subTitle: message,
      buttons: ["OK"]
    }).present();
  }

  submitScores() {

    let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });
    loader.present();

    const user_data = {
      location: this.location,
      date: this.date,
      email: this.current_user_email,
      opponent_id: this.from_play_game,
      opponent_name: this.player_username,
      opponent_scores: this.opponent_scores,
      user_scores: this.user_scores

    };

    this.db.list('scores').push(user_data).then(() => {
      this.alert("Scored Submitted Successfully!");
      this.navCtrl.pop()
      loader.dismiss();

    });
  }

}
