import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
// import { HomePage } from '../home/home';



// @IonicPage()
@Component({
  selector: 'page-sport-settings',
  templateUrl: 'sport-settings.html',
})
export class SportSettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SportSettingsPage');
    
  }

  proceed(){

    let loader = this.loadingCtrl.create({
      content: "Please wait ...",
    });

    loader.present();
    
    this.navCtrl.push(TabsPage)
    loader.dismiss();
  }

}
