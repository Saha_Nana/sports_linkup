import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { PickSportsPage } from '../pages/pick-sports/pick-sports';
import { SportSettingsPage } from '../pages/sport-settings/sport-settings';
import { PlayGamePage } from '../pages/play-game/play-game';
import { ScheduleGamePage } from '../pages/schedule-game/schedule-game';
import { GameHistoryPage } from '../pages/game-history/game-history';
import { ChatPage } from '../pages/chat/chat';
import { ScheduleHistoryPage } from '../pages/schedule-history/schedule-history';
import { PutScoresPage } from '../pages/put-scores/put-scores';

// import { AngularFireModule } from '@angular/fire';
// import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import * as firebase from "firebase";


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

const firebaseAuth =  {
  apiKey: "AIzaSyB1V0fcB75yHBoZKYtSMl2wMoqpudnGiEo",
  authDomain: "sportslinkup-a5a57.firebaseapp.com",
  databaseURL: "https://sportslinkup-a5a57.firebaseio.com",
  projectId: "sportslinkup-a5a57",
  storageBucket: "sportslinkup-a5a57.appspot.com",
  messagingSenderId: "1046844218510"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    PickSportsPage,
    SportSettingsPage,
    PlayGamePage,
    ScheduleGamePage,
    GameHistoryPage,
    ChatPage,
    ScheduleHistoryPage,
    PutScoresPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAuth, 'sportsLinkup'),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    PickSportsPage,
    SportSettingsPage,
    PlayGamePage,
    ScheduleGamePage,
    GameHistoryPage,
    ChatPage,
    ScheduleHistoryPage,
    PutScoresPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
